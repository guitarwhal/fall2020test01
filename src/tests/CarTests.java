package tests;
import automobiles.Car;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CarTests {
    @Test
    void max_posTest() {
        Car car = new Car(1);
        assertEquals(100, car.getLocation()*2);
    }
    
    @Test
    void getSpeedTest() {
        Car car = new Car(5);
        assertEquals(5, car.getSpeed());
    }

    @Test
    void getLocationTest() {
        Car car = new Car(1);
        assertEquals(50, car.getLocation());
    }
    
    @Test
    void negativeSpeedExceptionTest() {
        try {
            Car car = new Car(-420); //shouldn't work, hehe 420
            fail("It should throw exception because of the negative speed.");
        }
        catch (IllegalArgumentException var) {
        }
        catch (Exception var) {
            fail("Wrong exception type");
        }
    }
    
    @Test
    void moveLeftTest() {
        Car car = new Car(5);
        car.moveLeft();
        assertEquals(45, car.getLocation());
    }
    
    @Test
    void moveRightTest() {
        Car car = new Car(5);
        car.moveRight();
        assertEquals(55, car.getLocation());
    }
    
    @Test
    void moveLeftMinPositionTest() {
        Car car = new Car(60);
        car.moveLeft();
        assertEquals(0, car.getLocation());
    }
    
    @Test
    void moveRightMaxPositionTest() {
        Car car = new Car(60);
        car.moveRight();
        assertEquals(100, car.getLocation());
    }
    
    @Test
    void accelerateTest() {
        Car car = new Car(5);
        car.accelerate();
        assertEquals(6, car.getSpeed());
    }
    
    @Test
    void stopTest() {
        Car car = new Car(5);
        car.stop();
        assertEquals(0, car.getSpeed());
    }
}