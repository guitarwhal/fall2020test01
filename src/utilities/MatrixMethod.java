package utilities;

public class MatrixMethod {
	public static int[][] duplicate(int[][] arr) {
		int[][] newArr = new int[arr.length][arr[0].length * 2];
		
		for(int i = 0; i < arr.length; i++) {
			for(int j = 0; j < arr[i].length; j++) {
				newArr[i][j*2] = arr[i][j];
				newArr[i][(j*2) + 1] = arr[i][j];
			}
		}
		
		return newArr;
	}
}