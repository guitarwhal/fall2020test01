package matrixTest;
import utilities.MatrixMethod;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MatrixTest {
    @Test
    void duplicateTest() {
    	int[][] arr = {{1,2,3},{4,5,6}};
    	int[][] duplicate = MatrixMethod.duplicate(arr);
    	int[][] proper = {{1,1,2,2,3,3},{4,4,5,5,6,6}};
        assertArrayEquals(duplicate, proper);
    }

}